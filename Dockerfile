ARG BASE_IMAGE=node:20.18.0-alpine

FROM $BASE_IMAGE AS build
WORKDIR /app

COPY .npmrc package.json package-lock.json tsconfig.json ./
# Conditional copy (https://stackoverflow.com/a/31532674)
COPY .np[m] .npm
COPY src src
COPY docs docs
RUN npm ci --omit=optional --ignore-scripts --audit=false --cache .npm --prefer-offline --progress=false && \
    npm run build-apidocs && \
    npm run build-minimal && \
    npm prune --omit=dev --omit=optional --audit=false --progress=false

FROM $BASE_IMAGE
WORKDIR /app

RUN apk --no-cache add dumb-init
COPY --from=build /app/dist /app/dist
COPY --from=build /app/docs/generated /app/docs/generated
COPY --from=build /app/node_modules /app/node_modules
COPY config config
COPY package.json ./

# Remove busybox links
RUN busybox --list-full | \
    grep -E "bin/ifconfig$|bin/ip$|bin/netstat$|bin/nc$|bin/poweroff$|bin/reboot$" | \
    sed 's/^/\//' | xargs rm -f

# Create a non-root user
RUN addgroup -S nonroot && \
    adduser -S nonroot -G nonroot -h /app -u 1001 -D && \
    chown -R nonroot /app

# Disable persistent history
RUN touch /app/.ash_history && \
    chmod a=-rwx /app/.ash_history && \
    chown root:root /app/.ash_history

USER nonroot

EXPOSE 3000
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["node", "-r",  "dotenv/config", "dist/index.js"]
