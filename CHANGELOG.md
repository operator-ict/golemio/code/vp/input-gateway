# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.9.0] - 2025-01-20

### Changed

-   Optimize CI/CD and build of Docker image ([core#126](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/126))
-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [2.8.1] - 2025-01-13

### Added

-   Jis infotexts router ([pid#441](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/441))

## [2.8.0] - 2025-01-13

-   No changelog

## [2.7.0] - 2024-12-17

### Changed

-   Disable Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Fixed

-   Memory leaks related to Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

## [2.6.1] - 2024-12-04

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [2.6.0] - 2024-11-21

### Changed

-   Azure Blob Storage managed identity env vars update ([infra#304](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/304))

## [2.5.9] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [2.5.8] - 2024-07-04

### Removed

-   `body-parser-csv` dependency ([ig#82](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/82))

### Security

-   Security update - braces

## [2.5.7] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.5.6] - 2024-04-29

-   No changelog

## [2.5.5] - 2024-04-24

### Fixed

-   Invalid incoming xml returns Error 400 correctly ([core#97](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/97))

## [2.5.4] - 2024-04-15

### Added

-   Expose static openapi docs files

## [2.5.3] - 2024-04-10

-   No changelog

## [2.5.2] - 2024-03-25

-   No changelog

## [2.5.1] - 2024-01-22

-   No changelog

## [2.5.0] - 2024-01-17

### Changed

-   Improve README.md
-   Update Swagger UI, improve OpenAPI docs

## [2.4.9] - 2023-12-20

### Fixed

-   Improve request aborted handling ([core#86](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/86))

## [2.4.8] - 2023-10-09

### Changed

-   Update openapi docs, change swagger-ui config

## [2.4.7] - 2023-09-27

-   No changelog

## [2.4.6] - 2023-09-18

### Fixed

-   Fix responseTime metric in RequestLogger ([core#73](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/73))

## [2.4.5] - 2023-08-23

### Changed

-   Generate OAS file via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [2.4.4] - 2023-08-07

-   No changelog

## [2.4.3] - 2023-07-31

### Changed

-   NodeJS 18.17.0

## [2.4.2] - 2023-07-17

### Fixed

-   Open telemetry initialization

## [2.4.1] - 2023-07-10

-   No changelog

## [2.4.0] - 2023-06-26

-   No changelog

## [2.3.9] - 2023-06-19

### Fixed

-   Body parser syntax error handle ([input-gateway#81](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/81))

## [2.3.8] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.3.7] - 2023-06-07

-   No changelog

### Added

-   new router for ropidgtfs ([pid#247](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/247))

## [2.3.6] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.3.5] - 2023-05-29

-   No changelog

## [2.3.4] - 2023-05-03

-   No changelog

## [2.3.3] - 2023-04-26

-   No changelog

## [2.3.2] - 2023-04-19

-   No changelog

## [2.3.1] - 2023-03-22

-   No changelog

## [2.3.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.2.0] - 2023-02-06

### Added

-   Introduce dependency injection

### Changed

-   Change storage provider from S3 to Azure blob storage ([input-gateway#2]([https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/-/issues/2]))

## [2.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [2.0.21] - 2023-01-04

### Added

-   Update @golemio/core containing dependency injection

### Changed

-   Fixed markdown links, removed section Versions

## [2.0.20] - 2022-12-03

-   No changelog

## [2.0.19] - 2022-10-13

### Changed

-   Update TypeScript to v4.7.2

### Removed

-   Remove dredd tests ([pid#179](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/179))

## [2.0.18] - 2022-10-04

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

