[![pipeline status](https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/badges/master/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/commits/master)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/badges/master/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/commits/master)

# Golemio VP Input Gateway

> Fork of [Input Gateway](https://gitlab.com/operator-ict/golemio/code/input-gateway) dedicated only to Public Transport data.

The Input Gateway serves as a pivotal component within the Golemio data platform system, providing a REST API for receiving (PUSH) data to be stored in the Golemio data platform.

For comprehensive documentation, please visit: [Golemio Input Gateway Documentation](https://operator-ict.gitlab.io/golemio/code/input-gateway/)

Refer to the API documentation at:

-   [Main (production) Public Transport Input API Documentation](https://api.golemio.cz/pid/input-gateway/docs/openapi/)
-   [Test (development) Public Transport Input API Documentation](https://rabin.golemio.cz/pid/input-gateway/docs/openapi/)

Developed by http://operatorict.cz
