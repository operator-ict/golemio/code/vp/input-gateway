import { ContainerToken, InputGatewayContainer } from "@golemio/core/dist/input-gateway/ioc";
import { IConfiguration, SaveRawDataMiddleware } from "@golemio/core/dist/input-gateway";

import { ErrorHandler, FatalError, GeneralError, HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import express, { Request, Response, NextFunction, RequestHandler, ErrorRequestHandler } from "@golemio/core/dist/shared/express";
import http from "http";
import sentry from "@golemio/core/dist/shared/sentry";
import swaggerUi, { JsonObject, SwaggerOptions } from "swagger-ui-express";

// Import core components
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { responseTimeMiddleware } from "@golemio/core/dist/input-gateway/helpers";
import { createLightship, Lightship } from "@golemio/core/dist/shared/lightship";
import { getServiceHealth, BaseApp, Service, IServiceCheck, ILogger } from "@golemio/core/dist/helpers";
import { initSentry, metricsService } from "@golemio/core/dist/monitoring";

// Import resources
import { vehiclePositionsRouter } from "@golemio/pid/dist/input-gateway/vehicle-positions";
import { RopidGtfsRouter } from "@golemio/pid/dist/input-gateway/ropid-gtfs/RopidGtfsRouter";
import { JisInfotextsRouter } from "@golemio/pid/dist/input-gateway/jis/JisInfotextsRouter";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

/**
 * Entry point of the application. Creates and configures an ExpressJS web server.
 */
export default class App extends BaseApp {
    public express: express.Application = express();
    public server?: http.Server;
    public metricsServer?: http.Server;
    public port: number;
    private lightship: Lightship;
    private log: ILogger;
    private readonly config: IConfiguration;

    /**
     * Run configuration methods on the Express instance
     * and start other necessary services (crons, database, middlewares).
     */
    constructor() {
        super();
        this.config = InputGatewayContainer.resolve<IConfiguration>(ContainerToken.Config);
        this.log = InputGatewayContainer.resolve<ILogger>(ContainerToken.Logger);
        this.port = parseInt(this.config.port || "3005", 10);

        this.lightship = createLightship({
            detectKubernetes: this.config.node_env !== "production",
            shutdownHandlerTimeout: this.config.lightship.handlerTimeout,
            gracefulShutdownTimeout: this.config.lightship.shutdownTimeout,
            shutdownDelay: this.config.lightship.shutdownDelay,
        });
        process.on("uncaughtException", (err: Error) => {
            this.log.error(err);
            this.lightship.shutdown();
        });
        process.on("unhandledRejection", (reason: any) => {
            this.log.error(reason);
            this.lightship.shutdown();
        });
    }

    /**
     * Start the application and run the server
     */
    public start = async (): Promise<void> => {
        try {
            this.express = express();
            initSentry(this.config.sentry, this.config.app_name, this.express);
            metricsService.init(this.config, this.log);
            await this.connections();
            this.middleware();
            this.routes();
            this.errorHandlers();
            metricsService.serveMetrics();
            this.server = http.createServer(this.express);
            // Setup error handler hook on server error
            this.server.on("error", (err: any) => {
                sentry.captureException(err);
                ErrorHandler.handle(new FatalError("Could not start a server", "App", err), this.log);
            });
            // Serve the application at the given port
            this.server.listen(this.port, () => {
                // Success callback
                this.log.info(`Listening at http://localhost:${this.port}/`);
            });
            this.lightship.registerShutdownHandler(async () => {
                await this.gracefulShutdown();
            });
            this.lightship.signalReady();
        } catch (err) {
            sentry.captureException(err);
            ErrorHandler.handle(err, this.log);
        }
    };

    /**
     * Graceful shutdown - terminate connections and server
     */
    private gracefulShutdown = async (): Promise<void> => {
        this.log.info("Graceful shutdown initiated.");
        await this.server?.close();
        if (this.config.sentry.enabled) {
            await sentry.close();
        }
        await AMQPConnector.disconnect();
    };

    private connections = async (): Promise<void> => {
        await AMQPConnector.connect();
    };

    /**
     * Set custom headers
     */
    private customHeaders = (_req: Request, res: Response, next: NextFunction) => {
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, HEAD");
        next();
    };

    /**
     * Bind middleware to express server
     */
    private middleware = (): void => {
        this.express.use((req, res, next) => {
            const beacon = this.lightship.createBeacon();
            res.on("finish", () => {
                beacon.die();
            });
            next();
        });

        // Logger middleware should be before any other middleware, because it measures response time
        this.express.use(InputGatewayContainer.resolve(ContainerToken.RequestLogger));

        // TODO: Sentry middleware cause memory issues related to
        //   https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90
        // this.express.use(sentry.Handlers.requestHandler() as RequestHandler);
        // this.express.use(sentry.Handlers.tracingHandler() as RequestHandler);

        this.express.use(metricsService.metricsMiddleware());

        this.express.use(
            InputGatewayContainer.resolve<SaveRawDataMiddleware>(ContainerToken.SaveRawDataMiddleware).getMiddleware()
        );

        this.express.use(
            responseTimeMiddleware(
                bodyParser.json({
                    limit: this.config.payload_limit_json, // Reject payload bigger than limit
                })
            )
        );
        this.express.use(
            responseTimeMiddleware(
                bodyParser.xml({
                    limit: this.config.payload_limit_xml, // Reject payload bigger than limit
                    xmlParseOptions: {
                        explicitArray: false, // Only put nodes in array if >1
                        normalize: true, // Trim whitespace inside text nodes
                        normalizeTags: false, // Transform tags to lowercase
                    },
                })
            )
        );

        this.express.use(this.commonHeaders);
        this.express.use(this.customHeaders);
    };

    private healthCheck = async () => {
        const description = {
            app: "Golemio Data Platform Input Gateway",
            version: this.config.app_version,
        };

        const services: IServiceCheck[] = [{ name: Service.RABBITMQ, check: AMQPConnector.isConnected }];

        const serviceStats = await getServiceHealth(services);

        return { ...description, ...serviceStats };
    };

    /**
     * Define express server routes
     */
    private routes = (): void => {
        const defaultRouter = express.Router();

        // base url route handler
        defaultRouter.get(["/", "/health-check", "/status"], async (_req: Request, res: Response, _next: NextFunction) => {
            try {
                const healthStats = await this.healthCheck();
                if (healthStats.health) {
                    return res.json(healthStats);
                } else {
                    return res.status(503).send(healthStats);
                }
            } catch (err) {
                return res.status(503);
            }
        });

        this.express.use("/", defaultRouter);
        this.express.use("/vehiclepositions", vehiclePositionsRouter);
        this.express.use("/ropidgtfs", new RopidGtfsRouter().router);
        this.express.use("/jis", new JisInfotextsRouter().router);

        // ApiDocs
        let swaggerOptions: SwaggerOptions = {
            docExpansion: "none", // expands nothing
        };

        const oasFileUrl = "/docs/static/vp-input-gateway/openapi.json";
        this.express.use(oasFileUrl, express.static("docs/generated/openapi.json"));
        swaggerOptions.url = oasFileUrl;
        this.express.use(
            "/docs/openapi",
            swaggerUi.serveFiles(undefined, { swaggerOptions }),
            swaggerUi.setup(undefined, {
                swaggerOptions,
                customSiteTitle: "Public Transport | Golemio Input Gateway API Documentation",
            })
        );
    };

    /**
     * Define error handling middleware
     */
    private errorHandlers = (): void => {
        this.express.use(sentry.Handlers.errorHandler({ shouldHandleError: () => true }) as ErrorRequestHandler);

        // Request aborted error
        this.express.use((err: any, req: Request, _res: Response, next: NextFunction) => {
            if (err.type === "request.aborted") {
                next(new GeneralError("Request aborted", "App", new Error(`Called ${req.method} ${req.url}`), 400));
            } else {
                next(err);
            }
        });

        // input XML validation error
        this.express.use((err: any, _req: Request, res: Response, next: NextFunction) => {
            if (err?.message?.includes("\nLine: ")) {
                next(new GeneralError("Validation error", "App", err, 400));
            } else {
                next(err);
            }
        });

        // Not found error - no route was matched
        this.express.use((req: Request, _res: Response, next: NextFunction) => {
            next(new GeneralError("Route not found", "App", new Error(`Called ${req.method} ${req.url}`), 404));
        });

        // Body parser errors
        this.express.use((err: any, _req: Request, res: Response, next: NextFunction) => {
            if (err instanceof SyntaxError) {
                next(new GeneralError("Validation error", "App", err, 400));
            } else {
                next(err);
            }
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const warnCodes = [400, 404];
            const error = HTTPErrorHandler.handle(err, this.log, warnCodes.includes(err.status) ? "warn" : "error");
            if (error) {
                this.log.silly("Error caught by the router error handler.");
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    };
}
