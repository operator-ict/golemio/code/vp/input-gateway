// load reflection
import "@golemio/core/dist/shared/_global";

// load telemetry before all deps
import { initTraceProvider } from "@golemio/core/dist/monitoring";
initTraceProvider();

// start app
import App from "./App";
new App().start();
