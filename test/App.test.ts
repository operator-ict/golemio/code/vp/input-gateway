import App from "../src/App";
import { config } from "@golemio/core/dist/input-gateway/config";
import express from "@golemio/core/dist/shared/express";
import request from "supertest";
import sinon  from "sinon";
import * as chai from "chai";
import chaiAsPromised from "chai-as-promised"

const expect = chai.expect;

chai.use(chaiAsPromised);

describe("App", () => {
    let sandbox: any;
    let expressApp: express.Application;
    let app: App;

    before(async () => {
        app = new App();
        await app.start();
        expressApp = app.express;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(process, "exit");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should start", async () => {
        expect(app).not.to.be.undefined;
    });

    it("should have all config variables set", () => {
        expect(config).not.to.be.undefined;
    });

    it("should have health check on /", (done) => {
        request(expressApp)
            .get("/")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect((res: Response) => {
                expect(res.body).to.deep.include({
                    app: "Golemio Data Platform Input Gateway",
                    health: true,
                    services: [
                        {
                            name: "RabbitMQ",
                            status: "UP",
                        },
                    ],
                });
            })
            .expect(200, done);
    });

    it("should have health check on /health-check", (done) => {
        request(expressApp)
            .get("/health-check")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect((res: Response) => {
                expect(res.body).to.deep.include({
                    app: "Golemio Data Platform Input Gateway",
                    health: true,
                    services: [
                        {
                            name: "RabbitMQ",
                            status: "UP",
                        },
                    ],
                });
            })
            .expect(200, done);
    });

    it("should catch invalid XML errors from sax", (done) => {
        const badData = 'p<?xml version="1.0" encoding="utf-8"?>\n<m></m>';

        request(expressApp)
            .post("/vehiclepositions")
            .set("Accept", "application/json")
            .set("Content-Type", "text/xml")
            .send(badData)
            .expect("Content-Type", /json/)
            .expect((res: Response) => {
                expect(res.body).to.deep.include({
                    error_message: 'Bad request',
                    error_status: 400,
                    error_info: 'Non-whitespace before first tag.\nLine: 0\nColumn: 1\nChar: p'
                });
            })
            .expect(400, done);
    });
});
